/*
===============================================================================

	oculus_global.cpp
		Implements a weak Oculus abstraction for the FS2 game.

===============================================================================
*/

#include "oculus_public.h"
#include "OVR.h"
#include "graphics/2d.h"

using namespace OVR;
using namespace OVR::Util;
using namespace OVR::Util::Render;

/*
===============================================================================
	Global variables.
===============================================================================
*/

// Device variables.
static DeviceManager *OVR_device_manager = NULL;
static HMDInfo		OVR_device_info;
static HMDDevice *	OVR_device = NULL;			// NOTE: THIS WILL BE NULL IF THERE IS NO RIFT CONNECTED!

static SensorDevice *OVR_device_sensor = NULL;	// Provides access to raw sensor data.
static SensorFusion	*OVR_sensor_fusion = NULL;	// Provides access to high level sensor information.

// Renderer settings.
OculusView			Current_oculus_view = OCULUS_NONE;
static StereoConfig	OVR_config;

// View transforms.
float				OVR_half_ipd = 0.0f;
float				OVR_projection_center_offset = 0.0f;
float				OVR_x_center_offset = 0.0f;

bool				OVR_enabled = true;
bool				OVR_use_chromatic_shader = true;

/*
===============================================================================
	Private code.
===============================================================================
*/
static void ovr_create_device();
static void ovr_attach_sensor();
static void ovr_init_private_state();
static void ovr_set_dummy_device_info();
static void ovr_set_public_globals();

static void ovr_get_stereo_eye_params( OculusView view, StereoEyeParams *pParams );

/*
===============================================================================
ovr_init
	Finds and readies the Oculus device for use in game.
===============================================================================
*/
void ovr_init()
{
	// Attempt to connect to the Rift device.
	ovr_create_device();
	ovr_attach_sensor();

	// Ready the stereo helper class, device info, etc.
	ovr_init_private_state();

	// Ready IPD, projection offsets, etc.
	ovr_set_public_globals();
}

/*
===============================================================================
ovr_shutdown
	Prepares the Oculus device for shutdown.
===============================================================================
*/
void ovr_shutdown()
{
	if( OVR_sensor_fusion ) {
		delete OVR_sensor_fusion;
	}

	if( OVR_device_sensor ) {
		OVR_device_sensor->Release();
	}

	if( OVR_device )  {
		OVR_device->Release();
	}

	if( OVR_device_manager ) {
		OVR_device_manager->Release();
	}

	System::Destroy();
}

/*
===============================================================================
ovr_get_vertical_fov
	Calculates the vertical FOV for a single eye.
===============================================================================
*/
float ovr_get_vertical_fov()
{
	return OVR_config.GetYFOVRadians();
}

/*
===============================================================================
ovr_get_projection_matrix
	Builds the projection matrix for the given Oculus eye.
===============================================================================
*/
void ovr_get_projection_matrix( OculusView view, float near_z, float far_z, float matrix[16] )
{
	OVR_config.SetClippingPlanes( near_z, far_z );

	StereoEyeParams params;
	ovr_get_stereo_eye_params( view, &params );

	params.Projection.Transpose();
	memcpy( matrix, params.Projection.M, sizeof(params.Projection.M) );
}

/*
===============================================================================
ovr_get_view_matrix_offset
	Determines the view offset for the given Oculus eye.
===============================================================================
*/
void ovr_get_view_matrix_offset( OculusView view, float matrix[16] )
{
	StereoEyeParams params;
	ovr_get_stereo_eye_params( view, &params );

	params.ViewAdjust.Transpose();
	memcpy( matrix, params.ViewAdjust.M, sizeof(params.ViewAdjust.M) );
}

/*
===============================================================================
ovr_get_lens_center
	
===============================================================================
*/
void ovr_get_lens_center( float *pX, float *pY )
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( gr_screen.offset_x ) / i2fl( gr_screen.max_w );
	float fracY = i2fl( gr_screen.offset_y ) / i2fl( gr_screen.max_h );
	float fracW = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.max_w );
	float fracH = i2fl( gr_screen.clip_height ) / i2fl( gr_screen.max_h );

	*pX = fracX + ( fracW + OVR_x_center_offset * 0.5f ) * 0.5f;
	*pY = fracY + ( fracH * 0.5f );
}

/*
===============================================================================
ovr_get_screen_center
	
===============================================================================
*/
void ovr_get_screen_center( float *pX, float *pY )
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( gr_screen.offset_x ) / i2fl( gr_screen.max_w );
	float fracY = i2fl( gr_screen.offset_y ) / i2fl( gr_screen.max_h );
	float fracW = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.max_w );
	float fracH = i2fl( gr_screen.clip_height ) / i2fl( gr_screen.max_h );

	*pX = fracX + ( fracW * 0.5f );
	*pY = fracY + ( fracH * 0.5f );
}

/*
===============================================================================
ovr_get_distortion_scale
	Provides access to the Oculus computed distortion scales.
===============================================================================
*/
void ovr_get_distortion_scale( float *pX, float *pY )
{
	// Get the dimensions of the viewport as decimals.
	float fracW = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.max_w );
	float fracH = i2fl( gr_screen.clip_height ) / i2fl( gr_screen.max_h );

	float aspect = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.clip_height );

	float scale = 1.0f / OVR_config.GetDistortionScale();

	*pX = ( fracW * 0.5f ) * scale;
	*pY = ( fracH * 0.5f ) * scale * aspect;
}

/*
===============================================================================
ovr_get_distortion_scale_in
	
===============================================================================
*/
void ovr_get_distortion_scale_in( float *pX, float *pY )
{
	// Get the dimensions of the viewport as decimals.
	float fracW = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.max_w );
	float fracH = i2fl( gr_screen.clip_height ) / i2fl( gr_screen.max_h );

	float aspect = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.clip_height );

	*pX = ( 2.0f / fracW );
	*pY = ( 2.0f / fracH ) / aspect;
}

/*
===============================================================================
ovr_get_distortion_k
	
===============================================================================
*/
void ovr_get_distortion_k( float k[4] )
{
	k[0] = OVR_config.GetDistortionK( 0 );
	k[1] = OVR_config.GetDistortionK( 1 );
	k[2] = OVR_config.GetDistortionK( 2 );
	k[3] = OVR_config.GetDistortionK( 3 );
}

/*
===============================================================================
ovr_get_chomatic_aberration
	
===============================================================================
*/
void ovr_get_chomatic_aberration( float k[4] )
{
	const DistortionConfig &config = OVR_config.GetDistortionConfig();

	k[0] = config.ChromaticAberration[0];
	k[1] = config.ChromaticAberration[1];
	k[2] = config.ChromaticAberration[2];
	k[3] = config.ChromaticAberration[3];
}

/*
===============================================================================
ovr_get_distortion_matrix
	Builds the `Texm` matrix for the warp shader.
===============================================================================
*/
void ovr_get_distortion_matrix( float m[16] )
{
	// Get the dimensions of the viewport as decimals.
	float fracX = i2fl( gr_screen.offset_x ) / i2fl( gr_screen.max_w );
	float fracY = i2fl( gr_screen.offset_y ) / i2fl( gr_screen.max_h );
	float fracW = i2fl( gr_screen.clip_width ) / i2fl( gr_screen.max_w );
	float fracH = i2fl( gr_screen.clip_height ) / i2fl( gr_screen.max_h );

	float texm[16] = {
		fracW,	0.0f,	0.0f,	0.0f,
		0.0f,	fracH,	0.0f,	0.0f,
		0.0f,	0.0f,	0.0f,	0.0f,
		fracX,	fracY,	0.0f,	1.0f
	};

	memcpy( m, texm, sizeof(texm) );
}

/*
===============================================================================
ovr_get_orientation
	Provides access to the current direction of the Rift.
===============================================================================
*/
void ovr_get_orientation( angles *orient )
{
	if( !OVR_enabled || !OVR_device || !OVR_sensor_fusion ) {
		// No device attached. Therefore we have no angles.
		orient->b = orient->h = orient->p = 0.0f;
		return;
	}

	static float last_sensor_pitch = 0.0f;
	static float last_sensor_roll = 0.0f;
	static float last_sensor_yaw = 0.0f;

	// Get the raw orientation information.
	Quatf hmd_orientation = OVR_sensor_fusion->GetPredictedOrientation();
	float pitch = 0.0f;
	float roll = 0.0f;
	float yaw = 0.0f;

	// Convert the orientation information to FS2 angle types.
	hmd_orientation.GetEulerAngles<Axis_Y, Axis_X, Axis_Z>( &yaw, &pitch, &roll );
	orient->p = pitch - last_sensor_pitch;
	orient->b = roll - last_sensor_roll;
	orient->h = yaw - last_sensor_yaw;

	last_sensor_pitch = pitch;
	last_sensor_roll = roll;
	last_sensor_yaw = yaw;
}

/*
===============================================================================
	Private code
===============================================================================
*/

/*
===============================================================================
ovr_create_device
	Sets `ovr_device_info` to the default values stored within the Rift v1.
	This allows us to run FS2 with Rift rendering enabled without a physical
	Rift connected to the PC.
===============================================================================
*/
static void ovr_create_device()
{
	// Ready LibOVR for use.
	Log *pLog = Log::ConfigureDefaultLog( LogMask_All );
	System::Init( pLog );

	// Get a list of attached devices.
	OVR_device_manager = DeviceManager::Create();
	DeviceEnumerator<HMDDevice> pEnum = OVR_device_manager->EnumerateDevices<HMDDevice>();

	// Default to using the first device.
	OVR_device = pEnum.CreateDevice();
	if( !OVR_device ) {
		ovr_set_dummy_device_info();
		
		// Comment this line to run in Oculus rendering mode without an Oculus unit attached to the PC.
		//OVR_enabled = false;
	} else {
		OVR_device->GetDeviceInfo( &OVR_device_info );
	}
}

/*
===============================================================================
ovr_attach_sensor
	Initializes the Rift sensor helper objects.
===============================================================================
*/
static void ovr_attach_sensor()
{
	if( !OVR_device ) {
		// No device? No sensor.
		return;
	}

	OVR_device_sensor = OVR_device->GetSensor();
	if( OVR_device_sensor ) {
		OVR_sensor_fusion = new SensorFusion( OVR_device_sensor );
	}
}

/*
===============================================================================
ovr_set_dummy_device_info
	Sets `ovr_device_info` to the default values stored within the Rift v1.
	This allows us to run FS2 with Rift rendering enabled without a physical
	Rift connected to the PC.
===============================================================================
*/
static void ovr_set_dummy_device_info()
{
	OVR_device_info.HResolution = 1280;
	OVR_device_info.VResolution = 800;
	OVR_device_info.HScreenSize = 0.14975999f;
	OVR_device_info.VScreenSize = 0.093599997f;
	OVR_device_info.VScreenCenter = 0.046799999f;
	OVR_device_info.EyeToScreenDistance = 0.041000001f;
	OVR_device_info.LensSeparationDistance = 0.063500002f;
	OVR_device_info.InterpupillaryDistance = 0.064999998f;
	OVR_device_info.DistortionK[0] = 1.0f;
	OVR_device_info.DistortionK[1] = 0.22f;
	OVR_device_info.DistortionK[2] = 0.23999999f;
	OVR_device_info.DistortionK[3] = 0.0f;
	OVR_device_info.ChromaAbCorrection[0] = 0.99599999f;
	OVR_device_info.ChromaAbCorrection[1] = -0.0040000002f;
	OVR_device_info.ChromaAbCorrection[2] = 1.0140001f;
	OVR_device_info.ChromaAbCorrection[3] = 0.0f;
	OVR_device_info.DesktopX = 0;
	OVR_device_info.DesktopY = 0;
	OVR_device_info.DisplayDeviceName[0] = NULL;
	OVR_device_info.DisplayId = 0;
}

/*
===============================================================================
ovr_init_private_state
	Readies all private global variables for use.
===============================================================================
*/
static void ovr_init_private_state()
{
	// Set the stereo configuration settings.
	OVR_config.SetHMDInfo( OVR_device_info );
	OVR_config.SetFullViewport( Viewport(0,0, gr_screen.max_w, gr_screen.max_h) );
	OVR_config.SetStereoMode( Stereo_LeftRight_Multipass );
	OVR_config.SetDistortionFitPointVP( -1.0f, 0.0f );

	// Configure proper Distortion Fit.
	// For 7" screen, fit to touch left side of the view, leaving a bit of invisible
	// screen on the top (saves on rendering cost).
	// For smaller screens (5.5"), fit to the top.
	if( OVR_device_info.HScreenSize > 0.0f )
	{
		if( OVR_device_info.HScreenSize > 0.140f ) // 7"
			OVR_config.SetDistortionFitPointVP( -1.0f, 0.0f );
		else
			OVR_config.SetDistortionFitPointVP( 0.0f, 1.0f );
	}
}

/*
===============================================================================
ovr_set_public_globals
	Readies all public global variables for use.
===============================================================================
*/
static void ovr_set_public_globals()
{
	// Get the base eye offset.
	OVR_half_ipd = OVR_device_info.InterpupillaryDistance / 2.0f;

	// Calculate the eye projection offsets.
	const float viewCenter			= OVR_device_info.HScreenSize / 4.0f;
	const float eyeProjectionShift	= viewCenter -  (OVR_device_info.LensSeparationDistance / 2.0f );
	OVR_projection_center_offset	= 4.0f * eyeProjectionShift / viewCenter;

	const float lensOffset = OVR_device_info.LensSeparationDistance * 0.5f;
	const float lensShift = ( OVR_device_info.HScreenSize * 0.25f ) - lensOffset;
	OVR_x_center_offset = 4.0f * lensShift / OVR_device_info.HScreenSize;

	// Fudge the units to look nice in game.
	OVR_half_ipd = OVR_half_ipd * 10.0f;
	OVR_projection_center_offset = OVR_projection_center_offset * 5.0f;
}

/*
===============================================================================
ovr_get_stereo_eye_params
	Provides access to the eye params in the Rift stereo helper.
===============================================================================
*/
static void ovr_get_stereo_eye_params( OculusView view, StereoEyeParams *pParams )
{
	switch( view ) {
		case OCULUS_LEFT_VIEW:
			*pParams = OVR_config.GetEyeRenderParams( StereoEye_Left );
			break;

		case OCULUS_RIGHT_VIEW:
			*pParams = OVR_config.GetEyeRenderParams( StereoEye_Right );
			break;

		default:
		case OCULUS_NONE:
			*pParams = OVR_config.GetEyeRenderParams( StereoEye_Center );
			break;
	}
}