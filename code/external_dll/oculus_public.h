/*
===============================================================================

	oculus_public.h
		Implements a weak Oculus abstraction for the FS2 game.

===============================================================================
*/
#ifndef OCULUS_PUBLIC_H
#define OCULUS_PUBLIC_H

#include "globalincs/pstypes.h"

/*
===============================================================================
	Constants.
===============================================================================
*/
enum OculusView {
	OCULUS_NONE,
	OCULUS_LEFT_VIEW,
	OCULUS_RIGHT_VIEW,

	NUMBER_OF_OCULUS_VIEW_TYPES,
};

/*
===============================================================================
	Global variables.
===============================================================================
*/
extern float		OVR_half_ipd;
extern float		OVR_projection_center_offset;

extern OculusView	Current_oculus_view;

extern bool			OVR_enabled;
extern bool			OVR_use_chromatic_shader;

/*
===============================================================================
	Code.
===============================================================================
*/
	/*
	---------------------------------------------------------------------------
		Initialisation, destruction.
	---------------------------------------------------------------------------
	*/
	void		ovr_init();
	void		ovr_shutdown();

	/*
	---------------------------------------------------------------------------
		State accessors.
	---------------------------------------------------------------------------
	*/
	float		ovr_get_vertical_fov();
	void		ovr_get_projection_matrix( OculusView view, float near_z, float far_z, float matrix[16] );
	void		ovr_get_view_matrix_offset( OculusView view, float matrix[16] );
	void		ovr_get_orientation( angles *orient );

	/*
	---------------------------------------------------------------------------
		Warp shader data accessors.
	---------------------------------------------------------------------------
	*/
	void		ovr_get_lens_center( float *pX, float *pY );
	void		ovr_get_screen_center( float *pX, float *pY );
	void		ovr_get_distortion_scale( float *pX, float *pY );
	void		ovr_get_distortion_scale_in( float *pX, float *pY );
	void		ovr_get_distortion_k( float k[4] );
	void		ovr_get_chomatic_aberration( float k[4] );
	void		ovr_get_distortion_matrix( float m[16] );


#endif // OCULUS_PUBLIC_H